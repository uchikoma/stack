#!/bin/bash

COMPOSE_ARGs=""

for target in `echo element/{backend,database,opsys}.yml` ; do
    COMPOSE_ARGs="${COMPOSE_ARGs} -f ${target}"
done

for target in `echo runners/{nlp,iot,biz}.yml` ; do
    COMPOSE_ARGs="${COMPOSE_ARGs} -f ${target}"
done

alias transpose="docker-compose ${COMPOSE_ARGs} "

