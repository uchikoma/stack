#!/bin/bash

ufw allow in 22/tcp

apt update

apt install -y apt-transport-https ca-certificates curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

add-apt-repository --yes --update "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

apt-add-repository --yes --update "ppa:ansible/ansible"

apt update

apt -y upgrade

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

apt install -y lib{ffi,event}-dev nodejs python-{pip,wheel} nginx-full docker-ce ansible vagrant

